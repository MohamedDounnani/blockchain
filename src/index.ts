import { Block } from './models/block';
import { BlockChain } from './models/blockchain';

import * as moment from 'moment';

const blockchainExample = new BlockChain(5);
const data = {
    age: 24,
    name: 'Davide',
    surname: 'Marini'
};
const data2 = {
    age: 25,
    name: 'Alessandro',
    surname: 'Frant'
};

console.log('Mining block 1...');
blockchainExample.addBlock(new Block(1, data));
console.log('Mining block 2...');
blockchainExample.addBlock(new Block(2, data2));
console.log(+moment());

console.log('Is BlockChain valid? ' , blockchainExample.isChainIsValid() ? 'Yes' : 'No');

console.log(JSON.stringify(blockchainExample, null, 4));
